package com.wmlt.danmaku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DanmakuProvider {

    public static void main(String[] args) {
        SpringApplication.run(DanmakuProvider.class, args);
    }

}
