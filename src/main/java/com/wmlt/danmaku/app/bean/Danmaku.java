package com.wmlt.danmaku.app.bean;

import com.alibaba.fastjson.JSONAware;
import com.alibaba.fastjson.JSONObject;

import java.util.Objects;

/**
 * create by 1500255505@qq.com 2020/7/20 0:12
 */
public class Danmaku implements JSONAware {
    private Integer type;

    private Integer color;

    private String content;

    private String uName;
    private long sendTime;

    public static Danmaku fromJSON(JSONObject object) {
        Objects.requireNonNull(object);
        Danmaku danmaku = new Danmaku();
        danmaku.setContent(object.getString("text"));
        danmaku.setType(object.getInteger("type"));
        danmaku.setColor(object.getInteger("color"));
        danmaku.setUName(object.getString("uName"));
        danmaku.setSendTime(System.currentTimeMillis());
        return danmaku;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUName() {
        return uName;
    }

    public void setUName(String uName) {
        this.uName = uName;
    }

    public long getSendTime() {
        return sendTime;
    }

    public void setSendTime(long sendTime) {
        this.sendTime = sendTime;
    }

    @Override
    public String toJSONString() {
        JSONObject obj = new JSONObject();

        obj.put("text", content);
        obj.put("send_time", sendTime);
        obj.put("color", color);
        obj.put("type", type);
        obj.put("uName", uName);
        System.out.println(obj);
        return obj.toJSONString();
    }
}
